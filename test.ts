import test from "ava"
import { MultikeyMap } from "."
import deepEqual from "fast-deep-equal"

const a = () => [[1, 2, 3], 10] as [number[], number]
const b = () => [[1, 2, 4], 11] as [number[], number]
const c = () => [[], 12] as [number[], number]
const d = () => [[1, 6, 7], 13] as [number[], number]
const e = () => [[2, 1], 14] as [number[], number]

const keys = <K, V>(arr: [K, V][]) => arr.map(([k]) => k)
const values = <K, V>(arr: [K, V][]) => arr.map(([, v]) => v)

const map = new MultikeyMap<number[], number>()

map.set([1, 2, 3], 10)
map.set([1, 2, 4], 11)
map.set([], 12)
map.set([1, 6, 7], 13)
map.set([2, 1], 14)

test("set error", t => {
  t.throws(() => map.set(1 as any, 2))
})

test("size", t => {
  t.is(map.size, 5)
})

test("has", t => {
  t.true(map.has([1, 2, 3]))
  t.true(map.has([1, 2, 4]))
  t.true(map.has([]))
  t.true(map.has([1, 6, 7]))
  t.true(map.has([2, 1]))
  t.false(map.has([0]))
  t.throws(() => map.has(1 as any))
})

test("get", t => {
  t.is(map.get([1, 2, 3]), 10)
  t.is(map.get([1, 2, 4]), 11)
  t.is(map.get([]), 12)
  t.is(map.get([1, 6, 7]), 13)
  t.is(map.get([2, 1]), 14)
  t.is(map.get([0]), undefined)
  t.throws(() => map.get(1 as any))
})

const expected1 = [a(), b(), c(), d(), e()]

test("prefix entries", t => {
  t.true(arrayContentsDeepEqual(map.prefixEntries([1, 2]), [a(), b()]))
  t.true(arrayContentsDeepEqual(map.prefixEntries([1]), [a(), b(), d()]))
  t.true(arrayContentsDeepEqual(map.prefixEntries([2]), [e()]))
  t.true(arrayContentsDeepEqual(map.prefixEntries([]), expected1))
})

test("entries", t => {
  t.true(arrayContentsDeepEqual(map.entries(), expected1))
  t.true(arrayContentsDeepEqual(map[Symbol.iterator](), expected1))
  t.true(arrayContentsDeepEqual(map.keys(), keys(expected1)))
  t.true(arrayContentsDeepEqual(map.values(), values(expected1)))
})

test.serial("constructor", t => {
  const map2 = new MultikeyMap(map)
  t.true(arrayContentsDeepEqual(map2.entries(), expected1))

  const map3 = new MultikeyMap()
  t.is(map3.size, 0)
})

test.serial("delete", t => {
  const copy = new MultikeyMap(map)
  t.true(copy.delete([]))
  t.false(copy.delete([0]))
  t.is(copy.size, 4)
  t.true(arrayContentsDeepEqual(copy.entries(), [a(), b(), d(), e()]))
})

test.serial("clear", t => {
  const copy = new MultikeyMap(map)
  copy.clear()
  t.is(copy.size, 0)
  t.deepEqual(Array.from(copy.entries()), [])
})

function arrayContentsDeepEqual<T>(
  a: Iterable<T>,
  b: ReadonlyArray<T>
): boolean {
  const c = b.slice()
  for (let x of a) {
    let hasMatch = false
    for (let j = 0; j < c.length; j++) {
      const y = c[j]
      if (deepEqual(x, y)) {
        hasMatch = true
        c.splice(j, 1)
        break
      }
    }
    if (!hasMatch) {
      return false
    }
  }
  return true
}
