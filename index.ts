const EOA = Symbol("end of array")

export class MultikeyMap<K extends any[] = any[], V = any>
  implements Map<K, V> {
  private _size = 0
  // Base map recursively containing children multikey maps
  // any element of K => child MultikeyMap
  // EOA => V
  private baseMap = new Map<any, MultikeyMap<any[], V> | V>()

  /**
   * The number of elements in this map.
   */
  get size() {
    return this._size
  }

  constructor(source?: Iterable<[K, V]>) {
    if (source) {
      for (let [key, value] of source) {
        this.set(key, value)
      }
    }
  }

  get(key: K): V | undefined {
    checkIfArray(key)
    const { baseMap } = this
    if (key.length === 0) {
      return baseMap.get(EOA) as V | undefined
    } else {
      const [first, ...rest] = key
      if (baseMap.has(first)) {
        const child = this.baseMap.get(first)! as MultikeyMap<any[], V>
        return child.get(rest)
      } else {
        return undefined
      }
    }
  }

  setKV(key: K, value: V): boolean {
    checkIfArray(key)
    const { baseMap } = this
    let ret = false
    // if the key is empty
    if (key.length === 0) {
      // check if the value already exists
      if (!baseMap.has(EOA)) {
        // if the value doesn't exist, signal to update the size
        // of this node and ancestor nodes.
        ret = true
      }
      baseMap.set(EOA, value)
    } else {
      const [first, ...rest] = key
      ret = this.getOrCreateChildMap(first).setKV(rest, value)
    }
    if (ret) {
      this._size++
    }
    return ret
  }

  set(key: K, value: V): this {
    checkIfArray(key)
    this.setKV(key, value)
    return this
  }

  delete(key: K): boolean {
    checkIfArray(key)
    // signal to update the size of this node and ancestor nodes
    let ret = false
    const { baseMap } = this
    // if the key is empty
    if (key.length === 0) {
      ret = baseMap.delete(EOA)
    } else {
      const [first, ...rest] = key
      // if a child map exists
      if (baseMap.has(first)) {
        // child map exists; recurse
        const child = baseMap.get(first)! as MultikeyMap<any[], V>
        ret = child.delete(rest)
        // clean up if the child map has no more elements
        if (child.size === 0) {
          baseMap.delete(first)
        }
      } else {
        // child map doesn't exist; do not bother
        ret = false
      }
    }
    if (ret) {
      this._size--
    }
    return ret
  }

  has(key: K): boolean {
    checkIfArray(key)
    const { baseMap } = this
    if (key.length === 0) {
      return baseMap.has(EOA)
    } else {
      const [first, ...rest] = key
      // if a child map exists
      if (baseMap.has(first)) {
        const child = baseMap.get(first)! as MultikeyMap<any[], V>
        return child.has(rest)
      } else {
        // child map doesn't exist; end
        return false
      }
    }
  }

  private getOrCreateChildMap(k: ElementOf<K>): MultikeyMap<any[], V> {
    const { baseMap } = this
    if (baseMap.has(k)) {
      return baseMap.get(k) as MultikeyMap<any[], V>
    } else {
      const n = new MultikeyMap<any[], V>()
      baseMap.set(k, n)
      return n
    }
  }

  clear(): void {
    this.baseMap.clear()
    this._size = 0
  }

  *prefixEntries(prefixKey: SubArray<K>): IterableIterator<[K, V]> {
    if (prefixKey.length === 0) {
      yield* this.entries()
    } else {
      const [first, ...rest] = prefixKey
      const { baseMap } = this
      if (baseMap.has(first)) {
        const child = this.baseMap.get(first)! as MultikeyMap<any[], V>
        for (let [key, value] of child.prefixEntries(rest)) {
          yield [[first, ...key] as K, value]
        }
      }
    }
  }

  *entries(): IterableIterator<[K, V]> {
    for (let [k, v] of this.baseMap) {
      if (k === EOA) {
        yield [[] as any[], v as V] as [K, V]
      } else {
        for (let [rest, nestedValue] of (v as MultikeyMap<
          any[],
          V
        >).entries()) {
          yield [[k, ...rest] as K, nestedValue as V] as [K, V]
        }
      }
    }
  }

  *keys() {
    for (let [key] of this.entries()) yield key
  }

  *values() {
    for (let [, value] of this.entries()) yield value
  }

  forEach(
    callbackfn: (value: V, key: K, map: this) => void,
    thisArg?: any
  ): void {
    for (let [key, value] of [...this.entries()]) {
      callbackfn.call(thisArg, value, key, this)
    }
  }

  [Symbol.iterator]() {
    return this.entries()
  }

  get [Symbol.toStringTag]() {
    return "MultikeyMap"
  }
}

export type ElementOf<T> = T extends (infer A)[] ? A : never

export type SubArray<T> = T extends (infer A)[] ? A[] : never

function checkIfArray(x: any): void {
  if (!Array.isArray(x)) {
    throw new TypeError("not an array")
  }
}
